[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Cyberpunk
Font=JetBrainsMono Nerd Font,11,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Name=Dirk
Parent=FALLBACK/

[Scrolling]
HistoryMode=2

[Terminal Features]
UrlHintsModifiers=268435456
