#!/bin/bash

ln -s ~/dotfiles/Xresources ~/.Xresources
ln -s ~/dotfiles/compton.conf ~/.config/compton.conf
ln -s ~/dotfiles/i3 ~/.config/i3

